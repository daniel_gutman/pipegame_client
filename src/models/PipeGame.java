package models;

import game.Pipe;

import java.io.Serializable;
import java.util.ArrayList;

public class PipeGame implements Serializable {
    private ArrayList<ArrayList<Pipe>> board;
    private int stepCount;
    private int timeElapsed;

    public PipeGame() {
        this.setBoard(new ArrayList());
        this.stepCount = 0;
        this.timeElapsed = 0;
    }

    public PipeGame(ArrayList<ArrayList<Pipe>> board) {
        this.setBoard(board);
        this.stepCount = 0;
        this.timeElapsed = 0;
    }

    public ArrayList<ArrayList<Pipe>> getBoard() {
        return board;
    }

    public void setBoard(ArrayList<ArrayList<Pipe>> board) {
        this.board = board;
    }

    public void rotate(int i, int j, int rotateCount) {
        this.board.get(i).get(j).rotate(rotateCount);
    }

    public  String toString() {
        String boardString = "";
        for (int i = 0; i < board.size(); i++) {
            for (int j = 0; j < board.size(); j++) {
                boardString += board.get(i).get(j).toString();
            }
        }
        return boardString;
    }
}

