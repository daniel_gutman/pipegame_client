package models;

import utils.Config;

public class SettingsModel {

  PipeGameSettings currentSetings;

  public PipeGameSettings getCurrentSetings() {
    return currentSetings;
  }

  public void saveSettings() {
    Config.getInstance().setServerIp(currentSetings.getServerIp());
    Config.getInstance().setServerPort(currentSetings.getServerPort());
    //add save theme

  }

  public void changeTheme(String templateName) {
    Config.getInstance().setSelectedTemplate(templateName);
    Theme t = new Theme(Config.getInstance().getAngularPipeImage(), Config.getInstance().getStraightPipeImage(), Config.getInstance().getBgMusic());
  }
}
