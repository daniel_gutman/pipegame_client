package models;

public class PipeGameSettings {

  static String serverIp;
  static String serverPort;
  static Theme selectedTheme;

  public String getServerIp() {
    return serverIp;
  }

  public String getServerPort() {
    return serverPort;
  }

  public void setSelectedTheme(Theme selectedTheme) {
    this.selectedTheme = selectedTheme;
  }

  public Theme getSelectedTheme() {
    return selectedTheme;
  }



}
