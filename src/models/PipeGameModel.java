package models;

import game.Pipe;
import game.PipeType;
import javafx.stage.FileChooser;
import utils.Config;
import utils.FileUtil;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.*;
import java.util.ArrayList;

/**
 * Created by maorshabo on 11/12/2018.
 */
public class PipeGameModel {
  public PipeGame currentGame;
  private PropertyChangeSupport support;
  public ServerAPI serverAPI;

  public PipeGameModel(int rows, int columns) {
    this.currentGame = new PipeGame(this.generateEmptyBoard(rows, columns));
    this.support = new PropertyChangeSupport(this);
  }

  public PipeGameModel(ArrayList<ArrayList<Pipe>> board) {
    this.currentGame = new PipeGame(board);
  }

  private ArrayList<ArrayList<Pipe>> generateEmptyBoard(int rows, int columns) {
    ArrayList<ArrayList<Pipe>> board = new ArrayList<>(rows);
    for (int i = 0; i < rows; i++) {
      board.add(i, new ArrayList<>(columns));
      for (int j = 0; j < columns; j++) {
        board.get(i).add(new Pipe(PipeType.ANGULAR, Config.getInstance().getAngularPipeImage()));
      }
    }
    return board;
  }

  public ArrayList<ArrayList<Pipe>> getBoard() {
    return this.currentGame.getBoard();
  }

  public void loadGame() { //TODO temp function
    FileChooser fc = new FileChooser();
    StringBuilder mat = new StringBuilder();
    fc.setTitle("open file");
    fc.setInitialDirectory(new File("./resource"));
    File choosen = fc.showOpenDialog(null);
    if (choosen != null) {
      try (FileReader fr = new FileReader(choosen)) {
        int i;
        while ((i = fr.read()) != -1) {
          mat = mat.append((char) i);
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    print_mat(mat);
  }

  public void print_mat(StringBuilder mat) {
    System.out.println(mat.toString());
  }

  public void saveGame(PipeGame currentGame) throws IOException {
    File choosen = FileUtil.choosenSaveFile();
    FileOutputStream fout = new FileOutputStream(choosen);
    ObjectOutputStream oos = new ObjectOutputStream(fout);
    oos.writeObject(currentGame);
    fout.close();
    oos.close();
  }

  public PipeGame loadBinFile() throws IOException, ClassNotFoundException {
    File choosen = FileUtil.choosenLoadFile();
    FileInputStream fin = new FileInputStream(choosen);
    ObjectInputStream ois = new ObjectInputStream(fin);
    currentGame = (PipeGame) ois.readObject();
    return currentGame;
  }

  public void handlePipeClick(int i, int j) {
    this.currentGame.rotate(i, j, 1);
  }

  /*public void showSolution(solusion) {
    //TODO change the exist board with solution board
  }*/

  public void getSolution() throws IOException {
    String currentGameString = this.currentGame.toString();
    serverSol = serverAPI.getSolution(currentGameString);
    for (int i = 0; i < serverSol; i++) {
      this.currentGame.rotate(serverSol[i][0], serverSol[i][1], serverSol[i][2]);
    }
  }

  public void newGame(int n,int m) {
    newGameBoard = serverAPI.newGame(n,m);
    ArrayList<ArrayList<Pipe>> newBoard = toArray(newGameBoard);
    this.currentGame.setBoard(newBoard);
  }

  public ArrayList<ArrayList<String>> toArray(String newGame){
    int rowCount = newGame.split(System.lineSeparator()).length;
    ArrayList<ArrayList<Pipe>> newBoard = new ArrayList<>(rowCount);
    String[] rows = newGame.split(System.lineSeparator());
    for (int i=0; i<rowCount; i++){
      String[] columns = rows[i].split("");
      int colCount = rows[i].split(System.lineSeparator()).length;
      for (int j=0; j<colCount; j++){
        newBoard.get(i).get(j).
      }
    }
    return newBoard;
  }

  public void addPropertyChangeListener(PropertyChangeListener pcl) {
    support.addPropertyChangeListener(pcl);
  }

  public void removePropertyChangeListener( .,m pcl) {
    support.removePropertyChangeListener(pcl);
  }

  public void setCurrentGame(PipeGame game) {
    support.firePropertyChange("game", this.currentGame, game);
    this.currentGame = currentGame;
  }
}






