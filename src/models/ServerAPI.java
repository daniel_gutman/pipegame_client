package models;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class ServerAPI {

  private int port;
  private String hostname;
  Socket socket;
  OutputStream output = null;
  InputStream input = null;

  ServerAPI(String hostname,int port){
    this.hostname=hostname;
    this.port=port;
  }

  public void connectToServer() {
    try {
      this.socket = new Socket(hostname, port);
      this.output = this.socket.getOutputStream();
      this.input = this.socket.getInputStream();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

//from server
  public void sendAndRecieveData(String str) {
    byte[] data = str.getBytes();
    try {
      output.write(data);
    } catch (IOException e) {
      e.printStackTrace();
    }
    try {
      this.input = (InputStream) socket.getInputStream();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  public String getSolution(String str) throws IOException {
    try {
      OutputStream os = socket.getOutputStream();
      OutputStreamWriter osw = new OutputStreamWriter(os);
      BufferedWriter bw = new BufferedWriter(osw);
      String request= "getSolution";
      bw.write(request);

    } catch (IOException e) {
      e.printStackTrace();
    }

    sendAndRecieveData(str);
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((input)));
    System.out.println(bufferedReader);
    return bufferedReader.toString();

  }

  public String startNewGame(String str) {
    try {
      OutputStream os = socket.getOutputStream();
      OutputStreamWriter osw = new OutputStreamWriter(os);
      BufferedWriter bw = new BufferedWriter(osw);
      String request = "startNewGame";
      bw.write(request);

    } catch (IOException e) {
      e.printStackTrace();
    }
    sendAndRecieveData(str);
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((input)));
    System.out.println(bufferedReader);
    return bufferedReader.toString();

  }


}
