package models;

import java.awt.image.BufferedImage;

import utils.Config;

import java.awt.image.BufferedImage;

public class Theme {

  private BufferedImage angularPipeImage;
  private BufferedImage straightPipeImage;
  private String music;


  public BufferedImage getAngular() {
    return angularPipeImage;
  }

  public void setAngular(BufferedImage angular) {
    this.angularPipeImage = angular;
  }

  public BufferedImage getStraight() {
    return straightPipeImage;
  }

  public void setStraight(BufferedImage straight) {
    this.straightPipeImage = straight;
  }

  public String getMusic() {
    return music;
  }

  public void setMusic(String music) {
    this.music = music;
  }

  public Theme(BufferedImage angular, BufferedImage straight, String music) {
    this.angularPipeImage = angular;
    this.straightPipeImage = straight;
    this.music = music;
  }
}


