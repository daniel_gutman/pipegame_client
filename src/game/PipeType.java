package game;

/**
 * Created by maorshabo on 11/12/2018.
 */
public enum PipeType {
  ANGULAR, STRAIGHT
}