package game;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.awt.image.BufferedImage;

/**
 * Created by maorshabo on 10/12/2018.
 */

public class Pipe {
  private PipeType type;
  private BufferedImage image;
  private int rotateCount;

  public Pipe(PipeType type, BufferedImage image) {
    this.type = type;
    this.image = image;
    this.rotateCount = 0;
  }

  public Pipe(PipeType type, BufferedImage image, int rotateCount) {
    this.type = type;
    this.image = image;
    this.rotateCount = rotateCount;
  }

  public PipeType getType() {
    return type;
  }

  public BufferedImage getImage() {
    return this.image;
  }

  public void rotate() {
    this.rotateCount++;
    if (this.type == PipeType.ANGULAR) {
      if (this.rotateCount == 4) {
        this.rotateCount = 0;
      }
    } else if (this.type == PipeType.STRAIGHT) {
      if (this.rotateCount == 2) {
        this.rotateCount = 0;
      }
    }
  }

  public void rotate(int rotateCount) {
    for (int i = 0; i < rotateCount; i++) {
      this.rotate();
    }
  }

  public int getRotateCount() {
    return this.rotateCount;
  }
}