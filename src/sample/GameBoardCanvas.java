package sample;

import game.Pipe;
import game.PipeType;
import javafx.animation.Animation;
import javafx.animation.AnimationTimer;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

import java.util.ArrayList;

/**
 * Created by maorshabo on 05/12/2018.
 */
public class GameBoardCanvas extends Canvas {

  ArrayList<ArrayList<Pipe>> board;
  AnimationTimer timer;

  public void setBoard(ArrayList<ArrayList<Pipe>> board) {
    this.board = board;
    this.redraw();
  }

  public ArrayList<ArrayList<Pipe>> getBoard() {
    return this.board;
  }

  public int getBoardHeight() {
    return this.board.size();
  }

  public int getBoardWidth() {
    return this.board.get(0).size();
  }

  public void redraw() {
    //create walls
    double W = getWidth();
    double H = getHeight();
    //w,h= size of each tile

    double pipeWidth = W / this.board.get(0).size(); //this.MazeDate[0].length)= num of col in MazeDate
    double pipeHeight = H / this.board.size(); ////this.MazeDate.length)= num of row in MazeDate

    GraphicsContext gc = getGraphicsContext2D();
    gc.clearRect(0, 0, W, H);

    for (int i = 0; i < this.board.size(); i++) {
      for (int j = 0; j < this.board.get(i).size(); j++) {
        Pipe p = (Pipe) this.board.get(i).get(j);
        if (p.getType() == PipeType.ANGULAR) {
          Image img = SwingFXUtils.toFXImage(p.getImage(), null);
          this.drawRotatedImage(gc, img, 90 * p.getRotateCount(), i * pipeWidth, j * pipeHeight, pipeWidth, pipeHeight);
//          gc.drawImage(img,  i * w, j * h, w, h);
        }
      }
    }
  }

  private void rotate(GraphicsContext gc, double angle, double px, double py) {
    Rotate r = new Rotate(angle, px, py);
    gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
  }

  private void drawRotatedImage(GraphicsContext gc, Image image, double angle, double tlpx, double tlpy, double width, double height) {
    gc.save(); // saves the current state on stack, including the current transform
    rotate(gc, angle, tlpx + width / 2, tlpy + height / 2);
    gc.drawImage(image, tlpx, tlpy, width, height);
    gc.restore(); // back to original state (before rotation)
//    gc.strokeRect(tlpx, tlpy, width, height);
  }

  @Override
  public double prefHeight(double width) {
    return width * this.board.size() / this.board.get(0).size();
  }

  @Override
  public double prefWidth(double height) {
    return height * this.board.get(0).size() / this.board.size();
  }

  @Override
  public void resize(double width, double height) {
    setWidth(width);
    setHeight(height);
    redraw();
  }

}