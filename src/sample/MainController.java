package sample;


import game.Pipe;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.PipeGameModel;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by maorshabo on 11/12/2018.
 */
public class MainController implements Initializable, PropertyChangeListener {

  @FXML
  GameBoardCanvas gameboard;

  PipeGameModel boardModel = new PipeGameModel(5, 5);

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    gameboard.setBoard(boardModel.getBoard());
    gameboard.setOnMouseClicked(event -> {
      double cellLength = gameboard.getWidth() / gameboard.getBoardWidth();
      double cellHeight = gameboard.getHeight() / gameboard.getBoardHeight();
      int cellx = (int) (event.getX() / cellLength);
      int celly = (int) (event.getY() / cellHeight);
      boardModel.handlePipeClick(cellx, celly);
      gameboard.setBoard(boardModel.currentGame.getBoard());
    });
  }

  public void loadGame() {
  }

  public void saveGame() {
    System.out.println("Save file");
  }

  @FXML
  private void settings() throws IOException {
    openWindow("../main/resources/screens/settingsWindow.fxml", "Settings");
  }

  public void newGame() {
    System.out.println("New game");
  }

  public void getSolution() {
    this.boardModel.getSolution();
  }

  public void scoreBoard() throws IOException {
    openWindow("../main/resources/screens/scoreBoard.fxml", "Score Board");
    //TableView tab = new TableView();
    ScoreBoard scoreBoard = new ScoreBoard("Daniel", 4, 4, 5, "20:00:00");
    //tab.getItems().add(scoreBoard);
  }

  public void exit() {
    System.exit(0);
  }

  public void openWindow(String fxmlPath, String title) throws IOException {
    FXMLLoader fXMLLoader = new FXMLLoader();
    fXMLLoader.setLocation(getClass().getResource(fxmlPath));
    Stage stage = new Stage();
    Scene scene = new Scene(fXMLLoader.load());
    stage.setScene(scene);
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.setTitle(title);
    stage.show();
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    try {
      if (evt.getPropertyName().equals("game")) {
        ArrayList<ArrayList<Pipe>> newBoard = (ArrayList<ArrayList<Pipe>>) evt.getNewValue();
        gameboard.setBoard(newBoard);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
