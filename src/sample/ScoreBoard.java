package sample;

import java.sql.Time;

public class ScoreBoard {

    private String playerName;
    private Integer levelRow;
    private Integer levelCol;
    private Integer stepsCount;
    private String time;

    public ScoreBoard(String playerName,Integer levelRow,Integer levelCol,Integer stepsCount,String time) {
        this.playerName = playerName;
        this.levelRow = levelRow;
        this.levelCol = levelCol;
        this.stepsCount = stepsCount;
        this.time = time;

    }
    public String getPlayerName() {
        return playerName;
    }
    public Integer getLevelRow() { return levelRow; }
    public Integer getLevelCol() {
        return levelCol;
    }
    public Integer getStepsCount() {
        return stepsCount;
    }
    public String getTime() {
        return time;
    }
}
