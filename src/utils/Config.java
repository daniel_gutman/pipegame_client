package utils;

import javafx.scene.image.Image;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by maorshabo on 11/12/2018.
 */
public class Config {
  private final String CONFIG_PATH = "src/utils/configs/";
  private final String RESOURCES_PATH = "src/main/resources/images/";
  private final String CONFIG_FILE_NAME = CONFIG_PATH + "config.xml";

  private final String SELECTED_TEMPLATE_KEY = "selectedTemplate";
  private final String SERVER_IP_KEY = "serverIp";
  private final String SERVER_PORT_KEY = "serverPort";

  private final String ANGULAR_PIPE_IMAGE_KEY = "angularPipeImage";
  private final String STRAIGHT_PIPE_IMAGE_KEY = "straightPipeImage";
  private final String BG_MUSIC_KEY = "bgMusic";

  private XMLConfiguration config;
  private PropertiesConfiguration templateConfig;
  private FileBasedConfigurationBuilder<XMLConfiguration> builder;
  private FileBasedConfigurationBuilder<PropertiesConfiguration> propBuilder;

  private static Config c = null;

  public Config() {
    Configurations configs = new Configurations();

    try {
      this.config = configs.xml(new File(CONFIG_FILE_NAME));
      this.loadTemplate(this.getSelectedTemplate());
    } catch (ConfigurationException cex) {
      System.out.println(cex.getMessage());
    }
  }

  public static Config getInstance() {
    if (c == null) {
      c = new Config();
    }
    return c;
  }

  private ArrayList<String> getTemplatesNames() {
    return new ArrayList<>();
  }

  private void loadTemplate(String templateFile) {
    Configurations configs = new Configurations();
    try {
      this.templateConfig = configs.properties(new File(CONFIG_PATH + templateFile));
    } catch (ConfigurationException e) {
      e.printStackTrace();
      this.templateConfig = new PropertiesConfiguration();
    }
  }

  private void saveConfig() {
    if (this.config != null) {
      try {
        builder.save();
      } catch (ConfigurationException e) {
        e.printStackTrace();
      }
    }
  }

  public BufferedImage getAngularPipeImage() {
    return this.loadImage(this.templateConfig.getString(ANGULAR_PIPE_IMAGE_KEY));
  }

  public BufferedImage getStraightPipeImage() {
    return this.loadImage(this.templateConfig.getString(STRAIGHT_PIPE_IMAGE_KEY));
  }

  private BufferedImage loadImage(String path) {
    try {
      return ImageIO.read(new File(RESOURCES_PATH + path));
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }

  public String getBgMusic() {
    return this.templateConfig.getString(BG_MUSIC_KEY);
  }

  public String getSelectedTemplate() {
    return this.config.getString(SELECTED_TEMPLATE_KEY);
  }

  public String getServerIp() {
    return this.config.getString(SERVER_IP_KEY);
  }

  public String getServerPort() {
    return this.config.getString(SERVER_PORT_KEY);
  }

  public void setSelectedTemplate(String selectedTemplate) {
    this.config.setProperty(SELECTED_TEMPLATE_KEY, selectedTemplate);
    this.saveConfig();
  }

  public void setServerIp(String serverIp) {
    this.config.setProperty(SERVER_IP_KEY, serverIp);
    this.saveConfig();
  }

  public void setServerPort(String serverPort) {
    this.config.setProperty(SERVER_PORT_KEY, serverPort);
    this.saveConfig();
  }
}
