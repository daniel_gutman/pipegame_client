package utils;

import javafx.stage.FileChooser;

import java.io.File;

/**
 * Created by maorshabo on 17/12/2018.
 */
public class FileUtil {

  public static File choosenLoadFile() {
    FileChooser fc = new FileChooser();
    fc.setTitle("open file");
    fc.setInitialDirectory(new File("./resource"));
    File choosen = fc.showOpenDialog(null);
    return choosen;
  }

  public static File choosenSaveFile() {
    FileChooser fc = new FileChooser();
    fc.setTitle("open file");
    fc.setInitialDirectory(new File("./resource"));
    File choosen = fc.showSaveDialog(null);
    return choosen;
  }

}
